var stations = [];

function load_metromap() {
    // var s = Snap(1200, 1100), back = s.image('back.png', 0, 0, 1200, 1100);
    var s = Snap('#metromap');


    function selection(id) {
        var cur_stat = [];
        stations.forEach(function (item) {
            if (item.name === id) {
                cur_stat.push(item);
            }
        });
        cur_stat.forEach(function (item) {
            if (item.selected === false) {
                item.st_select();
            } else {
                item.st_unselect();
            }
        })
    }

    var station_colors = {
        1: '#F81824',
        2: '#009C55',
        3: '#0352A1',
        4: '#009EE1',
        5: '#765B27',
        6: '#FDAA38',
        7: '#B02693',
        8: '#FBD906',
        9: '#ACADAF',
        10: '#B0CF36',
        11: '#60BCB7',
        11.5: '#60BCB7',
        12: '#89D5F2',
        14: '#FAABB0',
    };

    class Station {
        constructor(name, x, y, line, orientation, display_name) {
            this.name = name;
            this.x = x;
            this.y = y;
            this.line = line;
            this.orientation = orientation;
            this.selected = false;
            this.display_name = display_name;
            this.color = station_colors[this.line];

            this.station = s.circle(this.x, this.y, 4);
            this.station.attr({
                fill: "#ffffff",
                stroke: this.color,
                strokeWidth: 2,
                class: name,
            });
            this.station.click(function () {
                selection(this.attr('class'));
            });


            if (this.display_name === true) {
                this.station_name = s.text(this.x + 10, this.y + 2, this.name);
                this.station_name.attr({'font-size': 10, class: name, strokeWidth: 0.2,});
                this.station_name_dimentions = this.station_name.node.getBoundingClientRect();
                // about orientation
                //
                // -1: 0 with last symbol above station
                // 1: 0 with first symbol above station
                // 90: station.y=name.y, name to the right of the station
                // 270: station.y=name.y, name to the left of the station
                // 181: 180 with last symbol below station
                // 45: name starts at 45 degrees
                // 180: text below station with centre alignment
                // 225: name ends at 225 degrees
                // 135: name starts at 135 degrees
                // 179: 180 with first symbol below station
                if (this.orientation === 270) {
                    this.station_name.attr({x: this.x - this.station_name_dimentions.width - 8});
                } else if (this.orientation === -1) {
                    this.station_name.attr({x: this.x - this.station_name_dimentions.width, y: this.y - 10});
                } else if (this.orientation === 181) {
                    this.station_name.attr({x: this.x - this.station_name_dimentions.width + 8, y: this.y + 15});
                } else if (this.orientation === 45) {
                    this.station_name.attr({x: this.x + 10, y: this.y - 10});
                } else if (this.orientation === 180) {
                    this.station_name.attr({x: this.x - (this.station_name_dimentions.width / 2), y: this.y + 15});
                } else if (this.orientation === 225) {
                    this.station_name.attr({x: this.x - 10 - this.station_name_dimentions.width, y: this.y + 10});
                } else if (this.orientation === 135) {
                    this.station_name.attr({x: this.x + 10, y: this.y + 10});
                } else if (this.orientation === 179) {
                    this.station_name.attr({x: this.x, y: this.y + 10});
                } else if (this.orientation === 0) {
                    this.station_name.attr({x: this.x - (this.station_name_dimentions.width / 2), y: this.y - 10});
                }
                this.station_name.click(function () {
                    selection(this.attr('class'));
                });
            }


        }

        st_select() {
            this.selected = true;
            this.station.animate({fill: this.color}, 10);
            if (this.display_name === true) {
                this.station_name.animate({fill: 'red'}, 10);
            }
            this.okay = s.polyline(
                this.x - 3, this.y - 1,
                this.x, this.y + 2,
                this.x + 5, this.y - 5,
                this.x + 4, this.y - 5,
                this.x, this.y,
                this.x - 1, this.y - 1,
                this.x - 3, this.y - 1
            );
            this.okay.attr({
                fill: 'white',
                strokeWidth: 0.5,
                stroke: 'white',
                class: this.name,
            });

        }

        st_unselect() {
            this.selected = false;
            this.station.animate({fill: '#ffffff'}, 10);
            if (this.display_name === true) {
                this.station_name.animate({fill: "#000"}, 10);
            }
            this.okay.remove()

        }

    }

    // draw lines
    const line_width = 5;
    let line_14 = s.circle(634, 451, 285).attr({fill: 'white', strokeWidth: line_width, stroke: station_colors[14]});
    let line_5 = s.circle(634, 451, 177).attr({fill: 'white', strokeWidth: line_width, stroke: station_colors[5]});
    var line_1 = s.polyline(378, 798, 378, 716, 839, 248, 839, 238).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[1]
    });
    var line_2 = s.polyline(500, 95, 500, 261, 548, 310, 548, 401, 785, 640, 785, 869, 801, 881, 801, 897).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[2]
    });
    var line_3 = s.polyline(912, 296, 912, 355, 780, 487, 616, 487, 578, 526, 548, 526, 460, 436, 450, 436, 339, 542, 249, 542, 228, 518, 228, 272).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[3]
    });
    var line_4 = s.polyline(548, 500, 435, 387, 353, 387).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[4]
    });
    var line_4 = s.polyline(548, 500, 460, 412, 445, 412, 320, 528, 260, 528, 249, 515, 249, 426, 228, 396).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[4]
    });
    var line_6 = s.polyline(711, 103, 711, 477, 565, 627, 565, 886, 598, 913).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[6]
    });
    var line_7 = s.polyline(390, 103, 390, 325, 462, 325, 548, 413, 660, 413, 928, 685, 928, 752).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[7]
    });
    var line_8 = s.polyline(969, 416, 863, 523, 681, 522).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[8]
    });
    var line_8 = s.polyline(249, 806, 249, 614, 279, 556, 299, 556, 299, 520, 377, 446, 377, 356, 488, 241, 592, 241).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[8]
    });
    var line_9 = s.polyline(603, 103, 603, 289, 641, 330, 548, 425, 548, 513, 671, 637, 671, 925, 635, 950).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[9]
    });
    var line_10 = s.polyline(815, 881, 815, 663, 815, 595, 833, 575, 833, 510, 655, 330, 629, 302, 629, 197, 615, 184, 603, 184, 580, 168, 580, 103).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[10]
    });
    var line_11 = s.polyline(671, 816, 785, 785).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[11]
    });
    var line_11_5 = s.polyline(592, 246, 492, 246, 386, 356, 407, 377).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[11.5]
    });
    var line_12 = s.polyline(598, 925, 635, 962, 635, 1026).attr({
        fill: 'none',
        strokeWidth: line_width,
        stroke: station_colors[12]
    });


    // draw transfers
    var trans_opacity = 0.6;
    var trans = s.rect(228 - 6, 384 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(279 - 6, 542 - 6, 12, 26, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(355 - 6, 496 - 6, 12, 26, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(824 - 6, 238 - 6, 27, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(830 - 6, 257 - 6, 27, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(767 - 6, 321 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(711 - 6, 366 - 6, 12, 32, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(660 - 6, 413 - 6, 12, 28, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(624 - 6, 467 - 6, 12, 32, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(548 - 6, 500 - 6, 12, 55, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(506 - 6, 573 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(500 - 6, 187 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(603 - 6, 276 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(641 - 6, 330 - 6, 26, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(469 - 6, 261 - 6, 42, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(548 - 6, 296 - 6, 12, 26, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(548 - 6, 401 - 6, 12, 36, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(680 - 6, 509 - 6, 12, 37, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(430 - 6, 650 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(746 - 6, 588 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(785 - 6, 681 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(671 - 6, 734 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(785 - 6, 773 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(801 - 6, 881 - 6, 26, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(459 - 6, 412 - 6, 12, 36, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(810 - 6, 458 - 6, 12, 40, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(901 - 6, 352 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(354 - 6, 387 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(407 - 6, 377 - 6, 12, 22, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(711 - 6, 163 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(711 - 6, 280 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(711 - 6, 465 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(565 - 6, 614 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(565 - 6, 728 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(598 - 6, 913 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(635 - 6, 950 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(390 - 6, 265 - 6, 40, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(377 - 6, 325 - 6, 52, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(493 - 6, 344 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(781 - 6, 522 - 6, 12, 38, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(833 - 6, 575 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(918 - 6, 466 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(833 - 6, 510 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(365 - 6, 356 - 6, 34, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(437 - 6, 293 - 6, 22, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    // ebuchaya savelovskaya - xz kak ee normal'no narisovat'
    var trans = s.rect(580 - 6, 244 - 6, 35, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(603 - 6, 154 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(603 - 6, 184 - 6, 24, 12, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(671 - 6, 624 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(671 - 6, 816 - 6, 12, 24, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(815 - 6, 660 - 6, 12, 25, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });
    var trans = s.rect(580 - 6, 156 - 6, 12, 26, 5).attr({
        stroke: 'black',
        strokeWidth: 1,
        fill: 'grey',
        opacity: trans_opacity
    });

    // define stations
    var stat = new Station('Планерная', 390, 103, 7, 270, true);
    stations.push(stat);
    stat = new Station('Сходненская', 390, 120, 7, 270, true);
    stations.push(stat);
    stat = new Station('Тушинская', 390, 137, 7, 270, true);
    stations.push(stat);
    stat = new Station('Спартак', 390, 154, 7, 270, true);
    stations.push(stat);
    stat = new Station('Щукинская', 390, 171, 7, 270, true);
    stations.push(stat);
    stat = new Station('Октябрьское поле', 390, 265, 7, 270, true);
    stations.push(stat);
    stat = new Station('Панфиловская', 418, 265, 14, -1, true);
    stations.push(stat);
    stat = new Station('Полежаевская', 390, 325, 7, -1, true);
    stations.push(stat);
    stat = new Station('Хорошёво', 377, 325, 14, 181, true);
    stations.push(stat);
    stat = new Station('Хорошёвская', 406, 325, 8, 45, true);
    stations.push(stat);
    stat = new Station('Хорошёвская', 416, 325, 11.5, 45, false);
    stations.push(stat);
    stat = new Station('Беговая', 462, 325, 7, 181, true);
    stations.push(stat);
    stat = new Station('Улица 1905 года', 477, 340, 7, 181, true);
    stations.push(stat);
    stat = new Station('Баррикадная', 493, 356, 7, 90, true);
    stations.push(stat);
    stat = new Station('Краснопресненская', 493, 344, 5, 90, true);
    stations.push(stat);
    stat = new Station('Пушкинская', 548, 413, 7, 270, true);
    stations.push(stat);
    stat = new Station('Тверская', 548, 401, 2, 270, true);
    stations.push(stat);
    stat = new Station('Чеховская', 548, 425, 9, 270, true);
    stations.push(stat);
    stat = new Station('Кузнецкий мост', 660, 413, 7, 90, true);
    stations.push(stat);
    stat = new Station('Лубянка', 660, 429, 1, 270, true);
    stations.push(stat);
    stat = new Station('Китай-город', 711, 465, 7, 90, true);
    stations.push(stat);
    stat = new Station('Китай-город', 711, 477, 6, 90, false);
    stations.push(stat);
    stat = new Station('Сретенский бульвар', 711, 386, 10, 90, true);
    stations.push(stat);
    stat = new Station('Чистые пруды', 711, 376, 1, 270, true);
    stations.push(stat);
    stat = new Station('Тургеневская', 711, 366, 6, 270, true);
    stations.push(stat);
    stat = new Station('Таганская', 781, 535, 7, 270, false);
    stations.push(stat);
    stat = new Station('Таганская', 781, 548, 5, 270, true);
    stations.push(stat);
    stat = new Station('Марксистская', 781, 522, 8, 270, true);
    stations.push(stat);
    stat = new Station('Третьяковская', 680, 522, 8, 270, true);
    stations.push(stat);
    stat = new Station('Третьяковская', 680, 509, 6, 270, false);
    stations.push(stat);
    stat = new Station('Новокузнецкая', 680, 534, 2, 270, true);
    stations.push(stat);
    stat = new Station('Сухаревская', 711, 312, 6, 270, true);
    stations.push(stat);
    stat = new Station('Проспект мира', 711, 280, 6, 90, true);
    stations.push(stat);
    stat = new Station('Проспект мира', 711, 292, 5, 90, false);
    stations.push(stat);
    stat = new Station('Рижская', 711, 233, 6, 90, true);
    stations.push(stat);
    stat = new Station('Алексеевская', 711, 216, 6, 90, true);
    stations.push(stat);
    stat = new Station('ВДНХ', 711, 197, 6, 90, true);
    stations.push(stat);
    stat = new Station('Ботанический сад', 711, 163, 6, 90, true);
    stations.push(stat);
    stat = new Station('Ботанический сад', 711, 175, 14, 90, false);
    stations.push(stat);
    stat = new Station('Свиблово', 711, 136, 6, 90, true);
    stations.push(stat);
    stat = new Station('Бабушкинская', 711, 120, 6, 90, true);
    stations.push(stat);
    stat = new Station('Медведково', 711, 103, 6, 90, true);
    stations.push(stat);
    stat = new Station('Алтуфьево', 603, 103, 9, 90, true);
    stations.push(stat);
    stat = new Station('Биберево', 603, 120, 9, 90, true);
    stations.push(stat);
    stat = new Station('Отрадное', 603, 137, 9, 90, true);
    stations.push(stat);
    stat = new Station('Владыкино', 603, 154, 9, 90, true);
    stations.push(stat);
    stat = new Station('Владыкино', 603, 166, 14, 90, false);
    stations.push(stat);
    stat = new Station('Тимирязевская', 603, 205, 9, 270, true);
    stations.push(stat);
    stat = new Station('Петровско-Разумовская', 603, 184, 9, 270, false);
    stations.push(stat);
    stat = new Station('Петровско-Разумовская', 615, 184, 10, 90, true);
    stations.push(stat);
    stat = new Station('Дмитровская', 603, 225, 9, 270, true);
    stations.push(stat);
    stat = new Station('Савеловская', 603, 244, 9, 270, false);
    stations.push(stat);
    stat = new Station('Савеловская', 592, 244, 11.5, 270, false);
    stations.push(stat);
    stat = new Station('Савеловская', 580, 244, 8, 270, true);
    stations.push(stat);
    stat = new Station('Новослободская', 603, 276, 5, 225, true);
    stations.push(stat);
    stat = new Station('Менделеевская', 603, 289, 9, 90, true);
    stations.push(stat);
    stat = new Station('Цветной бульвар', 641, 330, 9, 270, true);
    stations.push(stat);
    stat = new Station('Трубная', 655, 330, 10, 90, true);
    stations.push(stat);
    stat = new Station('Достоевская', 629, 265, 10, 90, true);
    stations.push(stat);
    stat = new Station('Марьина Роща', 629, 245, 10, 90, true);
    stations.push(stat);
    stat = new Station('Бутырская', 629, 225, 10, 90, true);
    stations.push(stat);
    stat = new Station('Фонвизинская', 629, 205, 10, 90, true);
    stations.push(stat);
    stat = new Station('Окружная', 580, 156, 10, 270, true);
    stations.push(stat);
    stat = new Station('Окружная', 580, 170, 14, 270, false);
    stations.push(stat);
    stat = new Station('Верхние Лихоборы', 580, 136, 10, -1, true);
    stations.push(stat);
    stat = new Station('Селигерская', 580, 103, 10, 270, true);
    stations.push(stat);
    stat = new Station('Речной вокзал', 500, 136, 2, 270, true);
    stations.push(stat);
    stat = new Station('Беломорская', 500, 111, 2, 270, true);
    stations.push(stat);
    stat = new Station('Ховрино', 500, 95, 2, 270, true);
    stations.push(stat);
    stat = new Station('Водный стадион', 500, 154, 2, 270, true);
    stations.push(stat);
    stat = new Station('Войковская', 500, 187, 2, 270, true);
    stations.push(stat);
    stat = new Station('Балтийская', 500, 199, 14, 270, true);
    stations.push(stat);
    stat = new Station('Сокол', 500, 215, 2, 270, true);
    stations.push(stat);
    stat = new Station('Аэропорт', 500, 231, 2, 270, true);
    stations.push(stat);
    stat = new Station('Динамо', 500, 261, 2, 90, true);
    stations.push(stat);
    stat = new Station('Петровский парк', 479, 261, 11.5, 180, true);
    stations.push(stat);
    stat = new Station('Петровский парк', 469, 261, 8, 180, false);
    stations.push(stat);
    stat = new Station('Белорусская', 548, 310, 2, 90, true);
    stations.push(stat);
    stat = new Station('Белорусская', 548, 296, 5, 90, false);
    stations.push(stat);
    stat = new Station('Маяковская', 548, 374, 2, 90, true);
    stations.push(stat);
    stat = new Station('Театральная', 624, 477, 2, 90, true);
    stations.push(stat);
    stat = new Station('Охотный ряд', 624, 467, 1, 90, true);
    stations.push(stat);
    stat = new Station('Площадь Революции', 624, 487, 3, 90, true);
    stations.push(stat);
    stat = new Station('Павелецкая', 746, 600, 2, 90, true);
    stations.push(stat);
    stat = new Station('Павелецкая', 746, 588, 5, 90, false);
    stations.push(stat);
    stat = new Station('Автозаводская', 785, 681, 2, 270, true);
    stations.push(stat);
    stat = new Station('Автозаводская', 785, 693, 14, 270, false);
    stations.push(stat);
    stat = new Station('Технопарк', 785, 743, 2, 270, true);
    stations.push(stat);
    stat = new Station('Коломенская', 785, 758, 2, 270, true);
    stations.push(stat);
    stat = new Station('Каширская', 785, 773, 2, 270, true);
    stations.push(stat);
    stat = new Station('Каширская', 785, 785, 11, 270, false);
    stations.push(stat);
    stat = new Station('Кантермировская', 785, 821, 2, 270, true);
    stations.push(stat);
    stat = new Station('Царицыно', 785, 837, 2, 270, true);
    stations.push(stat);
    stat = new Station('Орехово', 785, 853, 2, 270, true);
    stations.push(stat);
    stat = new Station('Домодедовская', 785, 869, 2, 270, true);
    stations.push(stat);
    stat = new Station('Красногвардейская', 801, 881, 2, 270, true);
    stations.push(stat);
    stat = new Station('Алма-Атинская', 801, 897, 2, 270, true);
    stations.push(stat);
    stat = new Station('Зябликово', 815, 881, 10, 90, true);
    stations.push(stat);
    stat = new Station('Шипиловская', 815, 865, 10, 90, true);
    stations.push(stat);
    stat = new Station('Борисово', 815, 848, 10, 90, true);
    stations.push(stat);
    stat = new Station('Марьино', 815, 832, 10, 90, true);
    stations.push(stat);
    stat = new Station('Братиславская', 815, 815, 10, 90, true);
    stations.push(stat);
    stat = new Station('Люблино', 815, 799, 10, 90, true);
    stations.push(stat);
    stat = new Station('Волжская', 815, 782, 10, 90, true);
    stations.push(stat);
    stat = new Station('Печатники', 815, 766, 10, 90, true);
    stations.push(stat);
    stat = new Station('Кожуховская', 815, 750, 10, 90, true);
    stations.push(stat);
    stat = new Station('Дубровка', 815, 660, 10, 90, true);
    stations.push(stat);
    stat = new Station('Дубровка', 815, 672, 14, 90, false);
    stations.push(stat);
    stat = new Station('Крестьянская застава', 833, 575, 10, 90, true);
    stations.push(stat);
    stat = new Station('Пролетарская', 833, 588, 7, 90, true);
    stations.push(stat);
    stat = new Station('Римская', 833, 510, 10, 90, true);
    stations.push(stat);
    stat = new Station('Площадь Ильича', 833, 523, 8, 90, true);
    stations.push(stat);
    stat = new Station('Чкаловская', 810, 486, 10, 90, true);
    stations.push(stat);
    stat = new Station('Курская', 810, 472, 5, 90, true);
    stations.push(stat);
    stat = new Station('Курская', 810, 458, 3, 90, true);
    stations.push(stat);
    stat = new Station('Бауманская', 833, 434, 3, 90, true);
    stations.push(stat);
    stat = new Station('Электрозаводская', 853, 414, 3, 90, true);
    stations.push(stat);
    stat = new Station('Электрозаводская', 887, 380, 3, 90, true);
    stations.push(stat);
    stat = new Station('Партизанская', 901, 365, 3, 90, true);
    stations.push(stat);
    stat = new Station('Измайлово', 901, 352, 14, 90, true);
    stations.push(stat);
    stat = new Station('Измайловская', 912, 330, 3, 90, true);
    stations.push(stat);
    stat = new Station('Первомайская', 912, 313, 3, 90, true);
    stations.push(stat);
    stat = new Station('Щелковская', 912, 296, 3, 90, true);
    stations.push(stat);
    stat = new Station('Авиамоторная', 891, 494, 8, 90, true);
    stations.push(stat);
    stat = new Station('Шоссе Энтузиастов', 918, 466, 8, 90, true);
    stations.push(stat);
    stat = new Station('Шоссе Энтузиастов', 918, 478, 14, 90, false);
    stations.push(stat);
    stat = new Station('Перово', 935, 449, 8, 90, true);
    stations.push(stat);
    stat = new Station('Новогиреево', 952, 433, 8, 90, true);
    stations.push(stat);
    stat = new Station('Новокосино', 969, 416, 8, 90, true);
    stations.push(stat);
    stat = new Station('Волгоградский проспект', 857, 612, 7, 90, true);
    stations.push(stat);
    stat = new Station('Текстильщики', 896, 653, 7, 90, true);
    stations.push(stat);
    stat = new Station('Кузьминки', 913, 670, 7, 90, true);
    stations.push(stat);
    stat = new Station('Рязанский проспект', 928, 685, 7, 90, true);
    stations.push(stat);
    stat = new Station('Выхино', 928, 702, 7, 90, true);
    stations.push(stat);
    stat = new Station('Лермонтовский проспект', 928, 719, 7, 90, true);
    stations.push(stat);
    stat = new Station('Жулебино', 928, 736, 7, 90, true);
    stations.push(stat);
    stat = new Station('Котельники', 928, 752, 7, 90, true);
    stations.push(stat);
    stat = new Station('Красные ворота', 734, 354, 1, 90, true);
    stations.push(stat);
    stat = new Station('Комсомольская', 767, 321, 1, 90, true);
    stations.push(stat);
    stat = new Station('Комсомольская', 767, 333, 5, 90, false);
    stations.push(stat);
    stat = new Station('Красносельская', 780, 308, 1, 90, true);
    stations.push(stat);
    stat = new Station('Сокольники', 797, 290, 1, 90, true);
    stations.push(stat);
    stat = new Station('Преображенская площадь', 813, 274, 1, 90, true);
    stations.push(stat);
    stat = new Station('Черкизовская', 830, 257, 1, 270, true);
    stations.push(stat);
    stat = new Station('Локомотив', 845, 257, 14, 90, true);
    stations.push(stat);
    stat = new Station('Бульвар Рокоссовского', 839, 238, 1, 90, true);
    stations.push(stat);
    stat = new Station('Бульвар Рокоссовского', 824, 238, 14, 90, false);
    stations.push(stat);
    stat = new Station('Библиотека имени Ленина', 548, 543, 1, 270, true);
    stations.push(stat);
    stat = new Station('Арбатская', 548, 526, 3, 270, true);
    stations.push(stat);
    stat = new Station('Боровитская', 548, 513, 9, 270, true);
    stations.push(stat);
    stat = new Station('Александровский сад', 548, 500, 4, 270, true);
    stations.push(stat);
    stat = new Station('Кропоткинская', 531, 560, 1, 90, true);
    stations.push(stat);
    stat = new Station('Парк культуры', 506, 585, 1, 270, true);
    stations.push(stat);
    stat = new Station('Парк культуры', 506, 573, 5, 270, false);
    stations.push(stat);
    stat = new Station('Фрунзенская', 471, 621, 1, 270, true);
    stations.push(stat);
    stat = new Station('Спортивная', 430, 662, 1, 270, true);
    stations.push(stat);
    stat = new Station('Лужники', 430, 650, 14, 270, true);
    stations.push(stat);
    stat = new Station('Воробьевы горы', 412, 681, 1, 270, true);
    stations.push(stat);
    stat = new Station('Университет', 394, 699, 1, 270, true);
    stations.push(stat);
    stat = new Station('Проспект Вернадского', 378, 716, 1, 270, true);
    stations.push(stat);
    stat = new Station('Юго-Западная', 378, 737, 1, 270, true);
    stations.push(stat);
    stat = new Station('Тропарево', 378, 757, 1, 270, true);
    stations.push(stat);
    stat = new Station('Румянцево', 378, 778, 1, 270, true);
    stations.push(stat);
    stat = new Station('Саларьево', 378, 798, 1, 270, true);
    stations.push(stat);
    stat = new Station('Смоленская', 499, 476, 3, 270, true);
    stations.push(stat);
    stat = new Station('Арбатская', 523, 475, 4, 90, true);
    stations.push(stat);
    stat = new Station('Смоленская', 499, 451, 4, 90, true);
    stations.push(stat);
    stat = new Station('Киевская', 459, 412, 4, 90, false);
    stations.push(stat);
    stat = new Station('Киевская', 459, 424, 5, 270, true);
    stations.push(stat);
    stat = new Station('Киевская', 459, 436, 3, 90, false);
    stations.push(stat);
    stat = new Station('Парк победы', 279, 542, 3, 90, false);
    stations.push(stat);
    stat = new Station('Парк победы', 279, 556, 8, 270, true);
    stations.push(stat);
    stat = new Station('Славянский бульвар', 228, 424, 3, 270, true);
    stations.push(stat);
    stat = new Station('Кунцевская', 228, 384, 3, 270, true);
    stations.push(stat);
    stat = new Station('Кунцевская', 228, 396, 4, 270, false);
    stations.push(stat);
    stat = new Station('Молодежная', 228, 368, 3, 270, true);
    stations.push(stat);
    stat = new Station('Крылатское', 228, 352, 3, 270, true);
    stations.push(stat);
    stat = new Station('Строгино', 228, 336, 3, 270, true);
    stations.push(stat);
    stat = new Station('Мякинино', 228, 320, 3, 270, true);
    stations.push(stat);
    stat = new Station('Волоколамская', 228, 304, 3, 270, true);
    stations.push(stat);
    stat = new Station('Митино', 228, 288, 3, 270, true);
    stations.push(stat);
    stat = new Station('Пятницкое шоссе', 228, 272, 3, 270, true);
    stations.push(stat);
    stat = new Station('Выставочная', 407, 387, 4, 90, true);
    stations.push(stat);
    stat = new Station('Деловой центр', 407, 377, 11.5, 90, false);
    stations.push(stat);
    stat = new Station('Международная', 354, 387, 4, 270, true);
    stations.push(stat);
    stat = new Station('Деловой центр', 354, 400, 14, 270, true);
    stations.push(stat);
    stat = new Station('Студенческая', 398, 456, 4, 90, true);
    stations.push(stat);
    stat = new Station('Кутузовская', 355, 496, 4, 90, false);
    stations.push(stat);
    stat = new Station('Кутузовская', 355, 509, 14, 90, true);
    stations.push(stat);
    stat = new Station('Фили', 249, 474, 4, 90, true);
    stations.push(stat);
    stat = new Station('Багратионовская', 249, 458, 4, 90, true);
    stations.push(stat);
    stat = new Station('Филевский парк', 249, 442, 4, 90, true);
    stations.push(stat);
    stat = new Station('Пионерская', 249, 426, 4, 90, true);
    stations.push(stat);
    stat = new Station('Октябрьская', 565, 627, 6, 270, false);
    stations.push(stat);
    stat = new Station('Октябрьская', 565, 614, 5, 270, true);
    stations.push(stat);
    stat = new Station('Шаболовская', 565, 674, 6, 270, true);
    stations.push(stat);
    stat = new Station('Ленинский проспект', 565, 740, 6, 270, true);
    stations.push(stat);
    stat = new Station('Площадь Гагарина', 565, 728, 14, 270, true);
    stations.push(stat);
    stat = new Station('Академическая', 565, 763, 6, 270, true);
    stations.push(stat);
    stat = new Station('Профсоюзная', 565, 783, 6, 270, true);
    stations.push(stat);
    stat = new Station('Новые Черемушки', 565, 804, 6, 270, true);
    stations.push(stat);
    stat = new Station('Калужская', 565, 824, 6, 270, true);
    stations.push(stat);
    stat = new Station('Беляево', 565, 844, 6, 270, true);
    stations.push(stat);
    stat = new Station('Коньково', 565, 865, 6, 270, true);
    stations.push(stat);
    stat = new Station('Теплый Стан', 565, 886, 6, 270, true);
    stations.push(stat);
    stat = new Station('Ясенево', 581, 899, 6, 270, true);
    stations.push(stat);
    stat = new Station('Новоясеневская', 598, 913, 6, 270, true);
    stations.push(stat);
    stat = new Station('Битцевский парк', 598, 925, 12, 270, true);
    stations.push(stat);
    stat = new Station('Лесопарковая', 614, 941, 12, 270, true);
    stations.push(stat);
    stat = new Station('Улица Старокачаловская', 635, 962, 12, 270, true);
    stations.push(stat);
    stat = new Station('Улица Скобелевская', 635, 979, 12, 270, true);
    stations.push(stat);
    stat = new Station('Бульвар Адмирала Ушакова', 635, 996, 12, 270, true);
    stations.push(stat);
    stat = new Station('Улица Горчакова', 635, 1011, 12, 270, true);
    stations.push(stat);
    stat = new Station('Бунинская аллея', 635, 1026, 12, 270, true);
    stations.push(stat);
    stat = new Station('Бульвар Дмитрия Донского', 635, 950, 9, 90, true);
    stations.push(stat);
    stat = new Station('Аннино', 671, 925, 9, 270, true);
    stations.push(stat);
    stat = new Station('Улица Академика Янгеля', 671, 901, 9, 179, true);
    stations.push(stat);
    stat = new Station('Пражская', 671, 877, 9, 270, true);
    stations.push(stat);
    stat = new Station('Южная', 671, 861, 9, 270, true);
    stations.push(stat);
    stat = new Station('Чертановская', 671, 845, 9, 270, true);
    stations.push(stat);
    stat = new Station('Севастопольская', 671, 828, 9, 270, true);
    stations.push(stat);
    stat = new Station('Каховская', 671, 816, 11.5, 270, true);
    stations.push(stat);
    stat = new Station('Нахимовский проспект', 671, 795, 9, 270, true);
    stations.push(stat);
    stat = new Station('Нагорная', 671, 774, 9, 270, true);
    stations.push(stat);
    stat = new Station('Нагатинская', 671, 747, 9, 270, true);
    stations.push(stat);
    stat = new Station('Верхние котлы', 671, 734, 14, 90, true);
    stations.push(stat);
    stat = new Station('Крымская', 631, 736, 14, 0, true);
    stations.push(stat);
    stat = new Station('ЗИЛ', 742, 715, 14, -1, true);
    stations.push(stat);
    stat = new Station('Тульская', 671, 674, 9, 270, true);
    stations.push(stat);
    stat = new Station('Серпуховская', 671, 637, 9, 270, true);
    stations.push(stat);
    stat = new Station('Добрынинская', 671, 624, 5, 90, true);
    stations.push(stat);
    stat = new Station('Полянка', 634, 600, 9, 90, true);
    stations.push(stat);
    stat = new Station('ЦСКА', 437, 293, 8, -1, true);
    stations.push(stat);
    stat = new Station('ЦСКА', 447, 293, 11.5, -1, false);
    stations.push(stat);
    stat = new Station('Шелепиха', 387, 356, 11.5, 270, false);
    stations.push(stat);
    stat = new Station('Шелепиха', 377, 356, 8, 270, false);
    stations.push(stat);
    stat = new Station('Шелепиха', 365, 356, 14, 270, true);
    stations.push(stat);
    stat = new Station('Минская', 249, 614, 8, 270, true);
    stations.push(stat);
    stat = new Station('Ломоносовский парк', 249, 634, 8, 270, true);
    stations.push(stat);
    stat = new Station('Раменки', 249, 655, 8, 270, true);
    stations.push(stat);
    stat = new Station('Мичуринский проспект', 249, 675, 8, 270, true);
    stations.push(stat);
    stat = new Station('Озерная', 249, 696, 8, 270, true);
    stations.push(stat);
    stat = new Station('Говорово', 249, 726, 8, 270, true);
    stations.push(stat);
    stat = new Station('Солнцево', 249, 746, 8, 270, true);
    stations.push(stat);
    stat = new Station('Боровское шоссе', 249, 766, 8, 270, true);
    stations.push(stat);
    stat = new Station('Новопеределкино', 249, 786, 8, 270, true);
    stations.push(stat);
    stat = new Station('Рассказовка', 249, 806, 8, 270, true);
    stations.push(stat);
    stat = new Station('Угрешская', 852, 635, 14, 90, true);
    stations.push(stat);
    stat = new Station('Новохохловская', 897, 561, 14, 90, true);
    stations.push(stat);
    stat = new Station('Нижегородская', 906, 536, 14, 90, true);
    stations.push(stat);
    stat = new Station('Андроновка', 914, 508, 14, 90, true);
    stations.push(stat);
    stat = new Station('Соколиная гора', 914, 397, 14, 90, true);
    stations.push(stat);
    stat = new Station('Белокаменная', 801, 219, 14, 90, true);
    stations.push(stat);
    stat = new Station('Ростокино', 775, 203, 14, 90, true);
    stations.push(stat);
    stat = new Station('Лихоборы', 549, 178, 14, -1, true);
    stations.push(stat);
    stat = new Station('Коптево', 527, 188, 14, 135, true);
    stations.push(stat);
    stat = new Station('Стрешнево', 437, 245, 14, -1, true);
    stations.push(stat);
    stat = new Station('Зорге', 400, 288, 14, 270, true);
    stations.push(stat);
    stat = new Station('Варшавская', 727, 801, 11, 90, true);
    stations.push(stat);

    // group selection: circle
    var circle_line_selected = false;
    var circle_line = s.text(900, 120, 'Все станции кольцевой').attr({'font-size': 14});
    circle_line.click(function () {
        if (circle_line_selected === false) {
            circle_line_selected = true;
            stations.forEach(function (item) {
                if (item.line === 5) {
                    item.st_select();
                }
            });
        } else {
            circle_line_selected = false;
            stations.forEach(function (item) {
                if (item.line === 5) {
                    item.st_unselect();
                }
            });
        }
    });

    // group selection: inside circle
    var stations_inside_circle = ['Красные ворота', 'Чистые пруды', 'Лубянка', 'Охотный ряд', 'Библиотека имени Ленина', 'Кропоткинская',
        'Маяковская', 'Тверская', 'Театральная', 'Новокузнецкая',
        'Площадь Революции', 'Арбатская', 'Смоленская',
        'Александровский сад',
        'Сухаревская', 'Китай-город', 'Третьяковская', 'Тургеневская',
        'Кузнецкий мост', 'Пушкинская',
        'Полянка', 'Боровитская', 'Чеховская', 'Цветной бульвар',
        'Трубная', 'Сретенский бульвар'];
    var in_circle_selected = false;
    var is_inside = 0;
    var in_circle = s.text(900, 140, 'Станции внутри кольца').attr({'font-size': 14});
    in_circle.click(function () {
        if (in_circle_selected === false) {
            in_circle_selected = true;
            stations.forEach(function (station) {
                if (stations_inside_circle.indexOf(station.name)!=-1) {
                    station.st_select();
                }
            });
        } else {
            in_circle_selected = false;
            stations.forEach(function (station) {
                if (stations_inside_circle.indexOf(station.name)!=-1) {
                    station.st_unselect();
                }
            });
        }
    });
}


function calculate(stations_list) {
    var stations_txt = '';
    stations_list.forEach(function (item) {
        if (item.selected === true) {
            stations_txt += item.name;
            stations_txt += ',';
        }
    });
    for (var i = 0; i < 200; i++) {
        stations_txt = stations_txt.replace(' ', '');
    }
    document.getElementById("metroresult").className = stations_txt;
}

// jquery easy mode
$(document).on('shown.bs.modal','#myModal', function () {
  load_metromap();
})